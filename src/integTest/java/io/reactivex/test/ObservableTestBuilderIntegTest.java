package io.reactivex.test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;

public class ObservableTestBuilderIntegTest {
    @Test
    public void onComplete() throws Exception {
        TestScheduler testScheduler = new TestScheduler();

        Observable<String> build = ObservableTestBuilder.<String>create(testScheduler).onNext("hans", 500, TimeUnit.MILLISECONDS)
                .onNext("wurst", 500, TimeUnit.MILLISECONDS)
                .onNext("isst", 200, TimeUnit.MILLISECONDS)
                .onNext("gerne", 300, TimeUnit.MILLISECONDS)
                .onNext("fleisch", 1, TimeUnit.SECONDS)
                .onComplete(0, TimeUnit.MILLISECONDS)
                .build();

        TestObserver<List<String>> testSubscriber = build.doOnSubscribe(disposable -> System.out.println("subscribed"))
                .doOnNext(s -> System.out.println("value"))
                .buffer(1001, TimeUnit.MILLISECONDS, testScheduler)
                .test();

        testScheduler.advanceTimeBy(5000, TimeUnit.MILLISECONDS);

        testSubscriber.await()
                .assertNoErrors();

        List<List<String>> values = testSubscriber.values();

        //TODO: TimeCapture
        // TODO: AssertHelper:
        // assert(onNext(t).at(RelativeTime(500)), onNext(t1).at(RelativeTime(500), onComplete().at(any()))

        assertThat(values.get(0), IsIterableContainingInOrder.contains("hans", "wurst"));
        assertThat(values.get(1), IsIterableContainingInOrder.contains("isst", "gerne"));
        assertThat(values.get(2), IsIterableContainingInOrder.contains("fleisch"));
    }

    @Test
    public void onErrorTest() throws Exception {
        TestScheduler testScheduler = new TestScheduler();

        Observable<String> build = ObservableTestBuilder.<String>create(testScheduler).onNext("hans", 500, TimeUnit.MILLISECONDS)
                .onNext("wurst", 500, TimeUnit.MILLISECONDS)
                .onError(new IllegalArgumentException("Error"), 1, TimeUnit.SECONDS)
                .build();

        TestObserver<String> test = build.test();

        testScheduler.advanceTimeBy(501, TimeUnit.MILLISECONDS);

        test.assertValueAt(0, "hans"::equals);

        testScheduler.advanceTimeBy(501, TimeUnit.MILLISECONDS);

        test.assertValueAt(1, "wurst"::equals);

        testScheduler.advanceTimeBy(1001, TimeUnit.MILLISECONDS);

        test.assertError(IllegalArgumentException.class);

        // TODO
        // TimeAsserter.create(testObserver, testScheduler)
        //      .advanceTime(...)
        //      .assertValue(...)
        //      .advanceTime(...)
        //      .advanceTime(...)
        //      .assertValues(1,2,3)
        //      .advanceTime()
        //      .assertComplete()

        // TODO
        // TimeAsserter.create(testScheduler)
        //      .advanceTime(150)
        //      .onNext("wurst", TimeUnit.Mil, 50)
        //      .onNext("hans", TimeUnit.Mil, 100)
        //      .assertComplete()
    }

    @Test
    public void never() throws Exception {
        TestScheduler testScheduler = new TestScheduler();
        Observable<String> obs = ObservableTestBuilder.<String>create(testScheduler).onNext("hans", 1000, TimeUnit.MILLISECONDS)
                .never()
                .build();

        TestObserver<String> test = obs.test();

        testScheduler.advanceTimeBy(1001, TimeUnit.MILLISECONDS);

        test.assertValueAt(0, "hans"::equals);

        testScheduler.advanceTimeBy(5000, TimeUnit.MILLISECONDS);

        test.assertNotComplete();
    }
}