package io.reactivex.test;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

class ObservableTestBuilder<T> {
    // TODO: onError: delaySubscription for Error vs Delay??
    // TODO: Do we need compose-operator?
    // TODO: Do we need burst() operator?

    // TODO: Hot / Cold

    // TODO: relativeTime / absoluteTime
    // 1. onNext("wurst", relative(500)) <- Will emit Value at Time 500 (relative to Now: 0)
    // 2. onNext("wurst", relative(200)) <- Will emit 'wurst' at Time 500+200 = 700 (relative to Time 500)
    // ---
    // 1. onNext("wurst", absolute(100)) <- Emit Value at Time 100 from NOW
    // 2. onNext("wurst", absolute(300)) <- Emit Value at Time 300 from NOW

    private final Scheduler scheduler;

    private final ArrayList<EventContainer> timeDependedEvents;

    private ObservableTestBuilder(Scheduler scheduler) {
        this.scheduler = scheduler;
        this.timeDependedEvents = new ArrayList<>();
    }

    public static <T> ObservableTestBuilder<T> create(Scheduler scheduler) {
        return new ObservableTestBuilder<T>(scheduler);
    }

    public OnNextStep<T> onNext(T value, long delay, TimeUnit unit) {
        EventContainer event = OnNextEvent.create(value, delay, unit);
        this.timeDependedEvents.add(event);

        return new OnNextStep<T>() {

            @Override
            public OnNextStep<T> onNext(T value, long delay, TimeUnit unit) {
                return ObservableTestBuilder.this.onNext(value, delay, unit);
            }

            @Override
            public FinishStep<T> onError(Throwable throwable, long delay, TimeUnit unit) {
                return ObservableTestBuilder.this.onError(throwable, delay, unit);
            }

            @Override
            public FinishStep<T> onComplete(long delay, TimeUnit unit) {
                return ObservableTestBuilder.this.onComplete(delay, unit);
            }

            @Override
            public FinishStep<T> never() {
                return ObservableTestBuilder.this.never();
            }
        };
    }

    public FinishStep<T> onError(final Throwable throwable, final long delay, final TimeUnit unit) {
        EventContainer event = OnErrorEvent.create(throwable, delay, unit);
        this.timeDependedEvents.add(event);

        return ObservableTestBuilder.this::build;
    }

    public FinishStep<T> onComplete(long delay, TimeUnit unit) {
        EventContainer event = OnCompleteEvent.create(delay, unit);
        this.timeDependedEvents.add(event);

        return ObservableTestBuilder.this::build;
    }

    private FinishStep<T> never() {
        EventContainer event = NeverEvent.create();
        this.timeDependedEvents.add(event);

        return ObservableTestBuilder.this::build;
    }

    private Observable<T> build() {
        if (timeDependedEvents.size() == 0) {
            throw new IllegalArgumentException("You must add an event.");
        }

        return Observable.fromIterable(timeDependedEvents)
                .reduce(Observable.<T>empty(), (tObservable, eventContainer) -> {

                    if (eventContainer instanceof OnNextEvent) {
                        return Observable.concat(tObservable, Observable.<T>timer(eventContainer.delay, eventContainer.unit, scheduler).map(aLong -> ((OnNextEvent<T>) eventContainer).value));
                    } else if (eventContainer instanceof OnErrorEvent) {
                        return Observable.concat(tObservable, Observable.<T>error(((OnErrorEvent) eventContainer).value).delaySubscription(eventContainer.delay, eventContainer.unit, scheduler));
                    } else if (eventContainer instanceof OnCompleteEvent) {
                        return Observable.concat(tObservable, Observable.<T>empty().delaySubscription(eventContainer.delay, eventContainer.unit, scheduler));
                    } else if (eventContainer instanceof NeverEvent) {
                        return Observable.concat(tObservable, Observable.never());
                    }

                    throw new IllegalArgumentException("Event is not of Type OnNextEvent|OnErrorEvent|OnCompleteEvent|NeverEvent");
                })
                .flatMapObservable(tObservable -> tObservable);
    }

    interface FinishStep<T> {
        Observable<T> build();
    }

    interface OnNextStep<T> {
        OnNextStep<T> onNext(T value, long delay, TimeUnit unit);

        FinishStep<T> onError(Throwable throwable, long delay, TimeUnit unit);

        FinishStep<T> onComplete(long delay, TimeUnit unit);

        FinishStep<T> never();
    }

    private static class EventContainer {
        private final long delay;
        private final TimeUnit unit;

        EventContainer(long delay, TimeUnit unit) {
            this.delay = delay;
            this.unit = unit;
        }
    }

    private static class OnNextEvent<T> extends EventContainer {
        final T value;

        private OnNextEvent(T value, long delay, TimeUnit unit) {
            super(delay, unit);

            this.value = value;
        }

        static <T> OnNextEvent<T> create(T value, long delay, TimeUnit unit) {
            return new OnNextEvent<T>(value, delay, unit);
        }
    }

    private static class OnCompleteEvent extends EventContainer {
        private OnCompleteEvent(long delay, TimeUnit unit) {
            super(delay, unit);
        }

        static <T> OnCompleteEvent create(long delay, TimeUnit unit) {
            return new OnCompleteEvent(delay, unit);
        }
    }

    private static class OnErrorEvent extends EventContainer {
        final Throwable value;

        private OnErrorEvent(Throwable value, long delay, TimeUnit unit) {
            super(delay, unit);

            this.value = value;
        }

        static OnErrorEvent create(Throwable throwable, long delay, TimeUnit unit) {
            return new OnErrorEvent(throwable, delay, unit);
        }
    }

    private static class NeverEvent extends EventContainer {
        private static NeverEvent DEFAULT = new NeverEvent(-1, null);

        private NeverEvent(long delay, TimeUnit unit) {
            super(delay, unit);
        }

        static NeverEvent create() {
            return DEFAULT;
        }
    }
}